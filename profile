#!/bin/bash

# Freetype2 subpixel hinting
# 0 = disabled, 1 = infinality, 2 = minimal
export FT2_SUBPIXEL_HINTING=1
# Possible values are 
# truetype:interpreter-version=35 (classic mode/2.6 default)
# truetype:interpreter-version=38 ("Infinality" mode)
# truetype:interpreter-version=40 (minimal - default)
export FREETYPE_PROPERTIES="truetype:interpreter-version=38"

# Custom directories
export USR_BIN_DIR="${XDG_LOCAL_DIR:=$HOME/.local}/bin"
export USR_GAME_DIR="${HOME}/games"
export USR_OPT_DIR="${HOME}/opt"
export USR_SRC_DIR="${HOME}/src"
export USR_SEC_DIR="${XDG_LOCAL_DIR:=$HOME/.local}/sec"
export USR_WEB_DIR="${HOME}/public_html"

#export LUTRIS_STEAM_RUNTIME="${HOME}/.local/steam_runtime"

# Setup environment for non-DE
#if [[ $DESKTOP_SESSION == "i3" ]]; then
# Qt uniform theme
# dependencies: qt5-styleplugins
export QT_QPA_PLATFORMTHEME='gtk2'

export DESKTOP_SESSION=gnome
export __USING_WM=1  # Custom definition for scripting
export GTK2_RC_FILES="${XDG_CONFIG_DIR:=${HOME}/.config}/gtk-2.0/config"

#fi

#export TERM="rxvt-unicode-256color"

# CCache path
export CCACHE_PATH="/usr/bin"
export CCACHE_DIR="${HOME}/.local/ccache"
export PATH="/usr/lib/colorgcc/bin:${PATH}"

# Android sdk location
export ANDROID_HOME="${HOME}/.local/android/sdk"
export ANDROID_SDK_HOME="${XDG_CONFIG_DIR:=~/.config}/android"  # sdk config dir
export PATH="${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/tools:${PATH}"

# Virtual environments
export PYENV_ROOT="${HOME}/.local/env/pyenv"
export RBENV_ROOT="${HOME}/.local/env/rbenv"
export NODENV_ROOT="${HOME}/.local/env/nodenv"
export GOENV_ROOT="${HOME}/.local/env/goenv"
export PATH="${PYENV_ROOT}/bin:${RBENV_ROOT}/bin:${NODENV_ROOT}/bin:${GOENV_ROOT}/bin:${PATH}"

eval "$(pyenv init -)"
eval "$(rbenv init -)"
eval "$(nodenv init -)"
eval "$(goenv init -)"

# golang environments
export GOPATH="${HOME}/.local/lib/go"
export PATH="${GOPATH}/bin:${PATH}"

# Set local bin on top of path
export PATH="${USR_BIN_DIR}/scripts:${PATH}"
export PATH="${USR_BIN_DIR}/launchers:${PATH}"
export PATH="${USR_BIN_DIR}:${PATH}"

# pacaur options
export AURDEST="${XDG_LOCAL_DIR:=$HOME/.local}/aur/pkgbuild"
export PKGDEST="${XDG_LOCAL_DIR:=$HOME/.local}/aur/pkg"
export SRCDEST="${XDG_LOCAL_DIR:=$HOME/.local}/aur/src"
export LOGDEST="${XDG_LOCAL_DIR:=$HOME/.local}/aur/log"
export BUILDDIR="${XDG_LOCAL_DIR:=$HOME/.local}/aur/build"

# Wine options
#export WINEARCH=win32
#export WINEPREFIX="${HOME}/.local/wine/win32"
export WINEDLLOVERRIDES="winemenubuilder.exe=d"

# Java environments
export JAVA_HOME="/usr/lib/jvm/default"
#export _JAVA_OPTIONS="-Djdk.gtk.version=3"
export _JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel"

# Locale
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8
export LC_NUMERIC=en_US.UTF-8
export LC_TIME=en_DK.UTF-8
export LC_COLLATE=en_US.UTF-8
export LC_MONETARY=en_US.UTF-8
export LC_MESSAGES=en_US.UTF-8
export LC_PAPER=en_GB.UTF-8
export LC_NAME=en_US.UTF-8
export LC_ADDRESS=en_US.UTF-8
export LC_TELEPHONE=en_US.UTF-8
export LC_MEASUREMENT=en_US.UTF-8
export LC_IDENTIFICATION=en_US.UTF-8

export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx

#SAL_USE_VCLPLUGIN=gen
#SAL_USE_VCLPLUGIN=kde4
export SAL_USE_VCLPLUGIN=gtk
#SAL_USE_VCLPLUGIN=gtk3

#[ -n "${XDG_SESSION_DESKTOP}" ] && xset m 2 0 2>/dev/null

