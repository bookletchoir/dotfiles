#!/usr/bin/env python3

import csv
import os
import subprocess
import sys
import time

EXT = ".tar.xz"
STATUS_FILE = "status.csv"

GPG_RECIPENT = '904FDB40CB2EA5F138A4915A41585CABDD0BB05C'
GPG_CMD = 'gpg --output {2} --encrypt --recipient {0} {1}'

CSV_HEADER = ["original_name", "archive_name", "extension",
              "compressed", "encrypted", "recipient_id"]


def compress_entry(input_name, output_fname) -> bool:
    """Compress `input_dname` to `output_fname`.

    If `output_fname` is already exists, try to get file list from existed
    archive file.
    If failed to get file list from archive, delete archive and process
    normally.

    :param input_name: directory name or path to compress
    :type input_name: str

    :param output_fname: output archive file
    :type output_fname: str

    :return: `True` if archive is created successfully. Otherwise, `False`.
    """
    log_info('Compressing: "{0}" -> "{1}"'.format(input_name, output_fname))

    # Check for existing file and test for corruption
    # If file existed and no corruptions found, assume it's done
    if os.path.isfile(output_fname):
        log_warn("File existed. Checking for corruption...")

        try:
            subprocess.check_output(['tar', '-tJf', output_fname],
                                    stderr=subprocess.DEVNULL)
            log_info("OK. Skipping...")
            return True
        except subprocess.CalledProcessError:
            log_warn("File corrupted. Recreating...")
            os.remove(output_fname)

    # Compress directory
    try:
        subprocess.check_call(['tar', '-cJf', output_fname, input_name],
                              stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        log_error("Failed to compress.")
        return False

    return True


def encrypt_file(input_fname, output_fname="", recipient_id=GPG_RECIPENT,
                 remove_source_on_completion=True) -> bool:
    """Encrypt `input_fname` to `output_fname` using gpg public key.

    If `input_fname` does not exist, return `False`.

    If `output_fname` already exist, remove and process normally.

    :param input_fname: File to encrypt
    :type input_fname: str

    :param output_fname: Encrypted file. Defaults to `<input_fname>.gpg`
    :type output_fname: str

    :param recipient_id: Recipient ID used for encryption.
    :type recipient_id: str

    :param remove_source_on_completion: remove target file on completion
    :type remove_source_on_completion: bool

    :return: `True` if encryption completed. Otherwise, `False`.
    """

    # Return False if input file does not exist.
    if not os.path.isfile(input_fname):
        log_error('File does not exists, skipping encryption: {0}'.format(
            input_fname
        ))
        return False

    # Set default output_fname if not specified
    if not output_fname:
        output_fname = "{0}.{1}".format(input_fname, "gpg")

    # Remove output file if it's already exist (possibly corrupted file)
    if os.path.isfile(output_fname):
        log_warn('File existed, removing: "{0}"'.format(output_fname))
        os.remove(output_fname)

    # Encrypt file
    try:
        log_info('Encrypting: "{0}" -> "{1}"'.format(input_fname, output_fname))
        subprocess.check_call(
            GPG_CMD.format(recipient_id, input_fname, output_fname).split(),
            stderr=subprocess.DEVNULL
        )
    except subprocess.CalledProcessError as e:
        log_error('Failed to encrypt "{0}": {1}'.format(input_fname, e))
        return False

    # Return false if there's no outpuf file
    if not os.path.isfile(output_fname):
        return True

    if remove_source_on_completion:
        os.remove(input_fname)
        log_info('Removed: "{0}"'.format(input_fname))

    return True


def process_entry(dataset, entry_id,
                  compress=True, encrypt=True) -> tuple([bool, bool]):
    """Process an entry of dataset: compression and encryption.

    Check for compression and encryption status and process if status is marked
    as False or "0" and update log file.

    :param dataset: Dataset from log file
    :type dataset: list[list[str, str, str, int, int, str]]

    :param entry_id: id of entry in dataset
    :type entry_id: int

    :param compress: compress target or not
    :type compress: bool

    :param encrypt: encrypt target or not
    :type encrypt: bool

    :return: Status of compression and encryption
    """
    orig_name, archive_name, file_ext, compressed, encrypted, recipient_id \
        = dataset[entry_id]

    # Compress phase
    if not compressed or compressed == "0":  # Not compressed
        compressed = compress_entry(orig_name, archive_name + file_ext)

        if compressed:
            update_csv(dataset, entry_id, compress_status=str(int(compressed)))
            encrypt = True
        else:
            encrypt = False
            log_info("Skipping encryption.")
    else:  # Already compressed
        encrypt = True
        log_info('Skipping compression for "{0}": '
                 'Already done (status report)'.format(orig_name))

    # Encrypt phase
    if encrypt:
        if not encrypted or encrypted == "0":
            encrypted = encrypt_file(archive_name + file_ext,
                                     recipient_id=GPG_RECIPENT)
            update_csv(dataset, entry_id,
                       encrypt_status=str(int(encrypted)),
                       recipient_id=GPG_RECIPENT)
        else:
            log_info(
                'Skipping encryption for "{0}": '
                'Already done (status report)'.format(archive_name + file_ext)
            )

    return compressed, encrypted


def process_root(root) -> None:
    """Perform full process of compression and encryption on each root.

    :param root: Directory to process
    :type root: str
    """

    # Check for directories in root
    os.chdir(root)

    entries = sorted([d for d in os.listdir(os.getcwd())])
    # remove status file from entries
    if STATUS_FILE in entries:
        entries.remove(STATUS_FILE)

    # Check for csv file
    # If no csv file, generate csv content
    status_file = os.path.join(root, STATUS_FILE)

    if os.path.isfile(status_file):

        dataset = parse_csv(status_file)

        # Appends unrecorded dirs to dataset
        dataset_new = gen_csv_content(
            [e_name for e_name in entries
             if not any(e_name == entry[0] for entry in dataset)],
            EXT
        )

        dataset += dataset_new

        # sort dataset
        dataset = sorted(dataset, key=lambda x: x[0])
    else:
        dataset = gen_csv_content(entries, EXT)

    # Init/update log file
    write_csv(dataset)

    # Generate unprocessed list of iters based on status
    list_unprocesed = [
        i for i, entry in enumerate(dataset) if not entry[3] or not entry[4]
    ]

    if not list_unprocesed:
        log_info("There's nothing to do.")
        return

    # List unprocess status
    log()
    log_info("To-do list:")
    log()

    for idx in list_unprocesed:
        name = dataset[idx][0]
        compress_status = dataset[idx][3]
        encrypt_status = dataset[idx][4]

        if not compress_status and not encrypt_status:
            log('"{0}": compress, encrypt'.format(name))
        elif not encrypt_status:
            log('"{0}" : encrypt'.format(name))
        else:
            log('{0}: (unexpected status received) compress={1} encrypt={2}'
                .format(name, compress_status, encrypt_status))

    log("{0}{1}{0}".format('\n', "-" * 30))

    p_status = []

    # Perform process on unprocessed iters
    for i, idx in enumerate(list_unprocesed):
        log()
        status = process_entry(dataset, idx)
        p_status.append(status)
        log_info("[{0}/{1}] Done".format(i + 1, len(list_unprocesed)))

    # Static failed processes

    if any(not p[0] or not p[1] for p in p_status):
        log("{0}{1}{0}".format('\n', "=" * 50))
        log_info("List of failed processes:\n")

        for i, entry in enumerate(p_status):
            c_status, e_status = entry[:]

            if not c_status or e_status:
                idx = list_unprocesed[i]
                name = dataset[idx][0]
                archive_name = dataset[idx][1] + dataset[idx][2]

                if not c_status and not e_status:
                    log('"{0}": compression failed, encryption failed'
                        .format(name))
                elif not c_status:
                    log('"{0}": compression failed'.format(name))
                elif not e_status:
                    log('"{0}": encryption failed'.format(archive_name))
                else:
                    log('"{0}": (unexpected status received) '
                        'compress={1}, encrypt={2}'
                        .format(name, c_status, e_status))


def gen_csv_content(dirs, ext) -> list:
    """Generate initial dataset from `dirs` and `ext`.

    Archive name will be generated automatically.
    Compression and encryption status will be default to 0.
    Recipient id will be default to empty str.

    :param dirs: list of directories
    :type dirs: list[str]

    :param ext: archive extension
    :type ext: str

    :return: Initial dataset
    """

    dataset = []  # type: list
    uid = time.strftime("%y%m%d_%H%M%S_{0}")  # type: str

    for i, dirname in enumerate(dirs):
        dataset.append([
            dirname,                              # original_dir
            uid.format(str(i).zfill(4)).upper(),  # archive_name
            ext,                                  # extension
            0,                                    # compression
            0,                                    # encryption
            ""                                    # recipient_id
        ])

    return dataset


def write_csv(dataset, filename=STATUS_FILE, header=CSV_HEADER,
              delimiter=";", quotechar="'") -> None:
    """Write `dataset` as csv file. Overwrite if file already exists.

    :param dataset: dataset to write
    :type dataset: list(list)

    :param filename: name of csv file
    :type filename: str

    :param header: Header of csv file
    :type header: list(str)

    :param delimiter: character to use as delimiter
    :type delimiter: str

    :param quotechar: character to use as quotechar
    :type quotechar: str

    :return: None
    """

    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=delimiter, quotechar=quotechar,
                            quoting=csv.QUOTE_MINIMAL)
        writer.writerow(header)                # Writes header
        [writer.writerow(d) for d in dataset]  # Writes content


def parse_csv(filename=STATUS_FILE, delimiter=";", quotechar="'") -> list:
    """Read `filename` as csv file and parse its content to dataset

    :param filename: file name or path to read
    :type filename: str

    :param delimiter: character to use as delimiter
    :type delimiter: str

    :param quotechar: character to use as quotechar
    :type quotechar: str

    :return: dataset
    """

    with open(filename, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiter, quotechar=quotechar)

        return [[row[0], row[1], row[2], int(row[3]), int(row[4]), row[5]]
                for row in list(reader)[1:]]


def update_csv(dataset, entry_id,
               compress_status="", encrypt_status="", recipient_id=""):

    if compress_status:
        dataset[entry_id][3] = compress_status

    if encrypt_status:
        dataset[entry_id][4] = encrypt_status

    if recipient_id:
        dataset[entry_id][5] = recipient_id

    write_csv(dataset)


def log(msg=""):
    print(msg)


def log_info(msg):
    log("{0} {1}".format("[INFO]", msg))


def log_warn(msg):
    log("{0} {1}".format("[WARNING]", msg))


def log_error(msg):
    log("{0} {1}".format("[ERROR]", msg))


def main():

    ####################
    # Set environments #
    ####################

    os.environ["XZ_DEFAULTS"] = "-T 7"  # Set 7 jobs to xz

    ###################
    # Parse arguments #
    ###################

    root_dirs = []

    if len(sys.argv) == 1:
        log_info("No args given, using CWD as root...")
        log()
        root_dirs.append(os.getcwd())
    else:
        root_dirs = ([os.path.abspath(d) for d in sys.argv[1:]
                      if os.path.isdir(d)])

    if not root_dirs:
        log_info("There's nothing to do.")
        sys.exit(0)

    ########################
    # Process on each root #
    ########################
    log_info("Selected directories: {0}".format(len(root_dirs)))
    [log(d) for d in root_dirs]
    log()

    for i, root in enumerate(sorted(root_dirs)):
        log_info('[{1}/{2}] Processing root: {0}'.format(
            root, i + 1, len(root_dirs))
        )
        process_root(root)
        log("{0}{1}{0}".format('\n', "=" * 60))

    pass


if __name__ == '__main__':
    main()
