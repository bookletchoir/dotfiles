# dotfiles
Arch Linux private dotfiles, for backup purpose

Some useful files you may want:
- `Xresouces` urxvt, xterm and rofi settings
- `vimrc` vim general-purpose settings
- `tmux` tmux config to play nice with urxvt
- `zshrc` zsh settings
- `local/bin/{dbrightness, dkeyboardled, dvol, touchpad-script}` some control
  scripts for laptop's media keys
- `local/bin/printscn` print screen scripts, use it with Scripts/imgur to upload
  screenshot
- `local/bin/imgur` upload file to imgur, does remember which file is uploaded
- `local/share/nemo/actions` Some custom nemo context menu actions

### Other dotfiles
- [i3wm](https://github.com/bookletchoir/i3wm-config)
- [zsh](https://github.com/bookletchoir/zsh.d)

### TODO
- [x] Be lazy

### Screenshot
![Screenshot](./screenshot.png)

