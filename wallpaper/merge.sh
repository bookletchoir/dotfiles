#!/bin/bash

image="wallpaper.png"
overlay="overlay.png"
image_scrlck="screenlock.png"

cd $(dirname "${BASH_SOURCE[0]}")
exec convert "${image}" "${overlay}" -gravity center -composite "${image_scrlck}"

